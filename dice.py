import random


def reset_dices(dices):
    for dice in dices:
        dice.reset()


def roll_unsaved_dices(dices):
    for dice in dices:
        if not dice.is_saved():
            dice.roll()


def saved_dices(dices):
    count = 0
    for dice in dices:
        if dice.is_saved():
            count += 1

    return count


def unsaved_dices(dices):
    return len(dices) - saved_dices(dices)


def get_unsaved_dice_indexes(dices):
    indexes = []
    for i, dice in enumerate(dices):
        if not dice.is_saved():
            indexes.append(i + 1)

    return indexes


def count_dice_by_value(value, dices):
    count = 0
    for dice in dices:
        if dice.get_rolled_value() == value:
            count += 1

    return count


class Dice:

    def __init__(self, symbols):
        self.saved = False
        self.values = []
        self.rolled_value = None

        symbol_keys = symbols.keys()

        for symbol_key in symbol_keys:
            for _ in range(symbols[symbol_key]):
                self.values.append(symbol_key)

    def reset(self):
        self.rolled_value = None
        self.saved = False

    def get_rolled_value(self):
        return self.rolled_value

    def is_saved(self):
        return self.saved

    def set_saved(self, value=True):
        self.saved = value

    def roll(self):
        self.rolled_value = random.choice(self.values)

        return self.rolled_value

    def __str__(self):
        return f"{'Saved' if self.saved else ''} {self.rolled_value}"

    def __repr__(self):
        return f"{'Saved' if self.saved else ''} {self.rolled_value}"
