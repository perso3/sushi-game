from dice import (count_dice_by_value, saved_dices)


def get_portions_by_symbol(symbol, sushis, aretes):
    result = None
    if symbol == "sushi":
        result = sushis
    elif symbol == "aretes":
        result = aretes

    return result


def get_max_portion_by_symbol(symbol, sushis, aretes):
    result = None
    portions = get_portions_by_symbol(symbol, sushis, aretes)
    for portion in portions:
        if result is None:
            result = portion
        elif result.get_value() < portion.get_value():
            result = portion

    return result


def get_available_portion(symbol, dices, sushis, aretes):
    result = None
    count = count_dice_by_value(symbol, dices)

    portions = get_portions_by_symbol(symbol, sushis, aretes)

    if saved_dices(dices) > 0:
        if 0 < count < len(portions) + 1:
            result = portions[count - 1]
        elif count == 0:
            result = None
        else:
            result = get_max_portion_by_symbol("aretes", sushis, aretes)

    return result


class Portion:

    def __init__(self, symbol, value):
        self.symbol = symbol
        self.value = value

    def get_symbol(self):
        return self.symbol

    def get_value(self):
        return self.value

    def __str__(self):
        return f"{self.value}"

    def __repr__(self):
        return f"{self.value}"
