import json
import random

from player import \
    (Player,
     get_the_scoreboard,
     get_player_by_id,
     get_other_players_ids,
     get_other_players,)

from dice import \
    (Dice,
     reset_dices,
     roll_unsaved_dices,
     saved_dices,
     unsaved_dices,
     count_dice_by_value,
     get_unsaved_dice_indexes,)

from portion import \
    (Portion,
     get_available_portion,)


class Board:

    def __init__(self):
        self.players = []
        self.dices = []
        self.sushis = []
        self.aretes = []

        with open('rules.json') as f:
            rules = json.load(f)

        player_rules = rules["players"]
        dice_rules = rules["dice"]
        portion_rules = rules["portion"]

        self.add_players(player_rules)
        self.create_dices(dice_rules)
        self.create_portions(portion_rules)

        self.show_portions()
        self.show_players()

        self.game()

    def add_players(self, player_rules):
        choice = 0
        min_players = player_rules["min"]
        max_players = player_rules["max"]
        while not (min_players <= choice <= max_players):
            choice = int(input(f"Nombre de joueurs (entre {min_players} et {max_players}) : "))

        names = []
        for i in range(choice):
            name = ""
            while name == "" or name in names:
                name = input(f"Nom du joueur {i + 1} : ")
            names.append(name)
            self.players.append(Player(i, name))

    def create_dices(self, dice_rules):
        count = dice_rules["count"]
        symbols = dice_rules["symbols"]

        for _ in range(count):
            self.dices.append(Dice(symbols))

    def create_portions(self, portion_rules):
        sushi_values = portion_rules["sushi"]
        aretes_values = portion_rules["aretes"]

        for value in sushi_values:
            self.sushis.append(Portion("sushi", value))
        random.shuffle(self.sushis)

        for value in aretes_values:
            self.aretes.append(Portion("aretes", value))
        random.shuffle(self.aretes)

    def game(self):
        while len(self.sushis) > 0 and len(self.aretes) > 0:
            for player in self.players:
                print("\n Changement de joueuer")
                print(player)

                reset_dices(self.dices)

                self.player_turn(player)

        self.show_scoreboard()

    def player_turn(self, player):
        tour = 4
        while tour > 0:
            choice = self.choose_action()

            if choice == 1:
                tour = self.action_roll_dice(tour)

            elif choice == 2:
                tour = self.action_pick_sushi(player, tour)

            elif choice == 3:
                tour = self.action_pick_aretes(player, tour)

            elif choice == 4:
                tour = self.action_steal_sushi(player, tour)

            elif choice == 5:
                tour = self.action_steal_aretes(player, tour)

    def action_steal_aretes(self, player, tour):
        if saved_dices(self.dices) == 0:
            print("\nVeuillez lancer les dés avant")
        else:
            count = count_dice_by_value("baguettes rouge", self.dices)

            if count < 3:
                print("Vous n'avez pas assez de baguettes rouge")
            elif count == 3:
                self.show_players_except(player.get_id())
                ids = get_other_players_ids(player.get_id(), self.players)
                choice = -1

                while choice not in ids:
                    choice = int(input(f"\nA qui voulez vous voler les aretes ?"
                                       f"\n{ids}-->"))
                    target_player = get_player_by_id(choice, self.players)
                    if target_player.count_sushi() == 0:
                        print("\nLe joueur cible n'a pas assez d'aretes")
                        choice = -1
                    else:
                        print(
                            f"\nVous avez volé des aretes {target_player.get_last_aretes()} "
                            f"à {target_player.get_name()}")

                        stolen_aretes = target_player.pop_aretes_at(-1)
                        player.add_to_aretes_list(stolen_aretes)

                        tour = 0

            elif count > 3:
                self.show_players_except(player.get_id())
                ids = get_other_players_ids(player.get_id(), self.players)
                choice = -1

                while choice not in ids:
                    choice = int(input(f"\nA qui voulez vous voler les aretes ?"
                                       f"\n{ids}-->"))
                    target_player = get_player_by_id(choice, self.players)
                    if target_player.count_aretes() == 0:
                        print("\nLe joueur cible n'a pas assez d'aretes")
                        choice = -1
                    else:
                        choice = 0
                        count_aretes = target_player.count_aretes()
                        while not (0 < choice <= count_aretes):
                            choice = int(input(f"\n{target_player.get_name()} a"
                                               f" {count_aretes} sushis le quel voulez"
                                               f" vous lui voler?"
                                               f"\nentre 1 et {count_aretes}-->"))

                        stolen_aretes = target_player.pop_aretes_at(choice - 1)

                        print(f"\nVous avez volé un sushi {stolen_aretes}"
                              f" à {target_player.get_name()}")

                        player.add_to_aretes_list(stolen_aretes)

                        tour = 0
        return tour

    def action_steal_sushi(self, player, tour):
        if saved_dices(self.dices) == 0:
            print("\nVeuillez lancer les dés avant")
        else:
            count = count_dice_by_value("baguettes bleu", self.dices)

            if count < 3:
                print("\nVous n'avez pas assez de baguettes bleu")
            elif count == 3:
                self.show_players_except(player.get_id())
                ids = get_other_players_ids(player.get_id(), self.players)
                choice = -1

                while choice not in ids:
                    choice = int(input(f"\nA qui voulez vous voler le sushi ?"
                                       f"\n{ids}-->"))
                    target_player = get_player_by_id(choice, self.players)
                    if target_player.count_sushi() == 0:
                        print("\nLe joueur cible n'a pas assez de sushis")
                        choice = -1
                    else:
                        print(f"\nVous avez volé un sushi {target_player.get_last_sushi()}"
                              f" à {target_player.get_name()}")

                        stolen_sushi = target_player.pop_sushi_at(-1)
                        player.add_to_sushi_list(stolen_sushi)

                        tour = 0

            elif count > 3:
                self.show_players_except(player.get_id())
                ids = get_other_players_ids(player.get_id(), self.players)
                choice = -1

                while choice not in ids:
                    choice = int(input(f"\nA qui voulez vous voler le sushi ?"
                                       f"\n{ids}-->"))
                    target_player = get_player_by_id(choice, self.players)
                    if target_player.count_sushi() == 0:
                        print("\nLe joueur cible n'a pas assez de sushis")
                        choice = -1
                    else:
                        choice = 0
                        count_sushi = target_player.count_sushi()
                        while not (0 < choice <= count_sushi):
                            choice = int(input(f"\n{target_player.get_name()} a"
                                               f" {count_sushi} sushis le quel voulez"
                                               f" vous lui voler?"
                                               f"\nentre 1 et {count_sushi}-->"))

                        stolen_sushi = target_player.pop_sushi_at(choice - 1)

                        print(f"\nVous avez volé un sushi {stolen_sushi}"
                              f" à {target_player.get_name()}")

                        player.add_to_sushi_list(stolen_sushi)

                        tour = 0
        return tour

    def action_pick_aretes(self, player, tour):
        if saved_dices(self.dices) == 0:
            print("\nVeuillez lancer les dés avant")
        else:
            portion = get_available_portion("aretes", self.dices, self.sushis, self.aretes)
            if portion is not None:
                self.aretes.remove(portion)
                player.add_to_aretes_list(portion)
                tour = 0
            else:
                print("Vous ne pouvez pas")
        return tour

    def action_pick_sushi(self, player, tour):
        if saved_dices(self.dices) == 0:
            print("\nVeuillez lancer les dés avant")
        else:
            portion = get_available_portion("sushi", self.dices, self.sushis, self.aretes)
            if portion is not None:
                self.sushis.remove(portion)
                player.add_to_sushi_list(portion)
                tour = 0
            else:
                print("Vous ne pouvez pas")
        return tour

    def action_roll_dice(self, tour):
        if unsaved_dices(self.dices) == 0 or tour == 1:
            print("\nVous ne pouvez plus relancer")
        else:
            roll_unsaved_dices(self.dices)
            self.show_rolled_dices()

            self.save_dices()
            tour -= 1
        return tour

    def choose_action(self):
        choice = 0
        self.show_portions()
        while not (1 <= choice <= 5):
            choice = int(input(f"\n1 - Lancer les dés"
                               f"\n2 - Prendre un sushi ({count_dice_by_value('sushi', self.dices)}"
                               f" dé(s) sushis)"
                               f"\n3 - Prendre des aretes "
                               f"({count_dice_by_value('aretes', self.dices)} dé(s) aretes)"
                               f"\n4 - Prendre un sushi chez un joueur"
                               f"({count_dice_by_value('baguettes bleu', self.dices)} dé(s) baguettes bleu)"
                               f"\n5 - Prendre des aretes chez un joueur"
                               f"({count_dice_by_value('baguettes rouge', self.dices)}"
                               f" dé(s) baguettes rouge)"
                               f"\n--> "))
        return choice

    def show_scoreboard(self):
        scoreboard = get_the_scoreboard(self.players)
        for i, player in enumerate(scoreboard):
            print(f"{i} - {player.get_name()} with {player.calculate_points()} points")

    def save_dices(self):

        dices_saved = 0
        loop = True

        while loop:
            choice = -1
            unsaved_dice_indexes = get_unsaved_dice_indexes(self.dices)
            unsaved_dice_indexes.append(0)

            while choice not in unsaved_dice_indexes:
                choice = int(input(f"\nChoisissez un dé non enregistré (soit 0 pour finir)"
                                   f"\n{unsaved_dice_indexes}"
                                   f"-->"))

            if choice == 0 and dices_saved != 0:
                loop = False
            elif choice == 0 and dices_saved == 0:
                print("\nVous devez garder au moins un dé")
            else:
                self.dices[choice - 1].set_saved()
                dices_saved += 1

    def show_portions(self):
        print(f"\nSushis : {self.sushis}")
        print(f"Aretes : {self.aretes}")

    def show_players(self):
        for player in self.players:
            print(player)

    def show_players_except(self, player_id):
        for player in get_other_players(player_id, self.players):
            print(player)

    def show_rolled_dices(self):
        for i, dice in enumerate(self.dices):
            print(f"\nDice n°{i + 1} : {dice}")
