def count_points(portions):
    points = 0
    for portion in portions:
        points += portion.get_value()

    return points


def get_the_scoreboard(players):
    scoreboard = sorted(players, key=lambda x: x.calculate_points(), reverse=True)
    return list(scoreboard)


def get_player_by_id(player_id, players):
    result = None
    for player in players:
        if player.get_id() == player_id:
            result = player
            break

    return result


def get_other_players_ids(player_id, players):
    result = []
    for player in players:
        if player.get_id() != player_id:
            result.append(player.get_id())

    return result


def get_other_players(player_id, players):
    result = []
    for player in players:
        if player.get_id() != player_id:
            result.append(player)

    return result


class Player:

    def __init__(self, player_id, name):
        self.id = player_id
        self.name = name
        self.sushi_list = []
        self.aretes_list = []

    def get_id(self):
        return self.id

    def get_name(self):
        return self.name

    def get_sushi_list(self):
        return self.sushi_list

    def add_to_sushi_list(self, sushi):
        self.sushi_list.append(sushi)

    def pop_sushi_at(self, index=-1):
        sushi = None
        if self.count_sushi() > 0:
            sushi = self.sushi_list.pop(index)
        return sushi

    def get_aretes_list(self):
        return self.aretes_list

    def add_to_aretes_list(self, aretes):
        self.aretes_list.append(aretes)

    def pop_aretes_at(self, index=-1):
        aretes = None
        if self.count_aretes() > 0:
            aretes = self.aretes_list.pop(index)
        return aretes

    def count_sushi(self):
        return len(self.sushi_list)

    def count_aretes(self):
        return len(self.aretes_list)

    def get_last_sushi(self):
        return self.sushi_list[-1] if len(self.sushi_list) != 0 else None

    def get_last_aretes(self):
        return self.aretes_list[-1] if len(self.aretes_list) != 0 else None

    def calculate_points(self):
        total_points = 0

        aretes_count = self.count_aretes()
        new_sushi_list = self.sushi_list[0:aretes_count]

        total_points += count_points(new_sushi_list)
        total_points += count_points(self.aretes_list)

        return total_points

    def __str__(self):
        return f"\nName : {self.name}\n" \
               f"Sushis : ({len(self.sushi_list)}) {self.get_last_sushi()}\n" \
               f"Aretes : ({len(self.aretes_list)}) {self.get_last_aretes()}"

    def __repr__(self):
        return f"\nName : {self.name}\n" \
               f"Sushi : ({len(self.sushi_list)}) {self.get_last_sushi()}\n" \
               f"Aretes : ({len(self.aretes_list)}) {self.get_last_aretes()} "
